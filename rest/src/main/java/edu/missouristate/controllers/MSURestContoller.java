package edu.missouristate.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.missouristate.model.Quote;

@Controller
public class MSURestContoller {

	@ResponseBody
    @GetMapping(value = { "/getquote" })
    public String getTestSpringBoot(Model model) throws JsonProcessingException {
        // Declare the service, service uri, and build the Rest Template
        String service = "Spring Boot";
        String serviceUri = "http://localhost:8080/getjsonquote";
        
        // Use a RestTemplate to communicate/handle the web request
        RestTemplate restTemplate = new RestTemplate();
        
        // Call the service and transform the JSON response into a Quote Object
        Quote quote = restTemplate.getForObject(serviceUri, Quote.class);
        
        // Format the response, add the details to the model and return the JSP
        ObjectMapper mapper = new ObjectMapper();
        String response = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(quote);
        
        model.addAttribute("service", service);
        model.addAttribute("serviceUri", serviceUri);
        model.addAttribute("response", response);

        //return "randomQuote";
        return response;
    }
    
    @ResponseBody
    @GetMapping(value="/getjsonquote")
    public String getMessage() {
    	return "{\"type\":\"success\",\"value\":{\"id\":10,\"quote\":\"Really loving Spring Boot, makes stand alone Spring apps easy.\"}}";
    }
    
}
