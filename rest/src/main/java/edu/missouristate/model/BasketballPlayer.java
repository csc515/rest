package edu.missouristate.model;

public class BasketballPlayer {
	private String firstName;
	private String lastName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "BasketballPlayer [firstName=" + firstName + ", lastName=" + lastName + "]";
	}

}
