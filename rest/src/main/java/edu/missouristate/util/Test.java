package edu.missouristate.util;

import com.google.gson.Gson;

import edu.missouristate.model.BasketballPlayer;

public class Test {

	public Test() {
		//testStringToObject();
		testObjectToJson();
	}

	private void testObjectToJson() {
		BasketballPlayer bp = new BasketballPlayer();
		bp.setFirstName("Larry");
		bp.setLastName("Bird");
		String json = new Gson().toJson(bp);
		System.out.println(json);
	}

	private void testStringToObject() {
		String json = "{\"firstName\" : \"Michael\", \"lastName\" : \"Jordan\"}";
		BasketballPlayer bp = new Gson().fromJson(json, BasketballPlayer.class);
		System.out.println(bp);
	}

	public static void main(String[] args) {
		new Test();
	}

}
