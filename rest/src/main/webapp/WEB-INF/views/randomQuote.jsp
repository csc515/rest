<%@ include file="include.jsp"%>
<!doctype html>
<html lang="en">
<head>
	<title>REST Example</title>
</head>
<body>
	<h1>RESTful Web Services Client</h1>
	<div>
		<b>Service: </b> ${service}
	</div>
	<div>
		<b>Service URI: </b> ${serviceUri}
	</div>
	<div>
		<b>Service Response: </b>
		<div>
			<pre>
				${response}
			</pre>
		</div>
	</div>
</body>
</html>